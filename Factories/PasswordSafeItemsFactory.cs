﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule.Models;
using SecurityModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Factories
{
    public class PasswordSafeItemsFactory
    {
        private Globals globals;


        public async Task<ResultItem<List<PasswordSafeItem>>> CreateEmptyPasswordSafeItemsList()
        {
            var taskResult = await Task.Run<ResultItem<List<PasswordSafeItem>>>(() =>
            {
                var result = new ResultItem<List<PasswordSafeItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<PasswordSafeItem>()
                };

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<PasswordSafeItem>>> CreatePasswordSafeItemsList(PasswordSafeRelItems passwordSafeRelItems, clsOntologyItem refParentItem)
        {
            var taskResult = await Task.Run<ResultItem<List<PasswordSafeItem>>>(() =>
            {
                var result = new ResultItem<List<PasswordSafeItem>>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = passwordSafeRelItems.PasswordsOfRefItem.Select(relItm => new PasswordSafeItem
                {
                    IdReference = passwordSafeRelItems.RefItem.GUID,
                    NameReference = passwordSafeRelItems.RefItem.Name,
                    IdParentReference = refParentItem.GUID,
                    NameParentReference = refParentItem.Name,
                    IdPassword = relItm.ID_Other,
                    Password = "*****"
                }).ToList();

                result.Result.AddRange(from userRel in passwordSafeRelItems.UsersRightLeft
                                       join passwordRel in passwordSafeRelItems.PasswordsOfUsersRightLeft on userRel.ID_Object equals passwordRel.ID_Object
                                       select new PasswordSafeItem
                                       {
                                           IdReference = passwordSafeRelItems.RefItem.GUID,
                                           NameReference = passwordSafeRelItems.RefItem.Name,
                                           IdParentReference = refParentItem.GUID,
                                           NameParentReference = refParentItem.Name,
                                           IdUser = userRel.ID_Object,
                                           NameUser = userRel.Name_Object,
                                           IdPassword = passwordRel.ID_Other,
                                           Password = "****"
                                       });

                result.Result.AddRange(from userRel in passwordSafeRelItems.UsersLeftRight
                                       join passwordRel in passwordSafeRelItems.PasswordsOfUsersLeftRight on userRel.ID_Other equals passwordRel.ID_Object
                                       select new PasswordSafeItem
                                       {
                                           IdReference = passwordSafeRelItems.RefItem.GUID,
                                           NameReference = passwordSafeRelItems.RefItem.Name,
                                           IdParentReference = refParentItem.GUID,
                                           NameParentReference = refParentItem.Name,
                                           IdUser = userRel.ID_Other,
                                           NameUser = userRel.Name_Other,
                                           IdPassword = passwordRel.ID_Other,
                                           Password = "****"
                                       });

                return result;
            });

            return taskResult;
        }

        public PasswordSafeItemsFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
