﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Models
{
    public class GetSecuritySessionsResult
    {
        public List<clsOntologyItem> SecuritySessions { get; set; }
        public List<clsObjectRel> SessionsToGroups { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionsToServers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> SessionsToUsers { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> GroupsToUsers { get; set; } = new List<clsObjectRel>();
    }
}
