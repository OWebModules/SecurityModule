﻿using OntologyClasses.BaseClasses;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        scrollable = true,
        resizable = true,
        selectable = SelectableType.row,
        editable = EditableType.False,
        height = "100%")]
    [KendoPageable(buttonCount = 5, pageSize = 30, pageSizes = new int[] { 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class PasswordSafeItem
    {
        [KendoColumn(hidden = true)]
        public string IdPassword { get; set; }

        [KendoColumn(hidden = false, Order = 0, filterable = false, title = "Password")]
        public string Password { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReference { get; set; }

        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Reference")]
        public string NameReference { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParentReference { get; set; }

        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Reference-Parent")]
        public string NameParentReference { get; set; }

        [KendoColumn(hidden = true)]
        public string IdUser { get; set; }

        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "User", template = "#= NameUser #<button class='copy-username' onClick='selectAndCopy(this)'><i class='fa fa-clipboard' aria-hidden='true'></i></button>")]
        public string NameUser { get; set; }

        private clsOntologyItem passwordItem;

        public void SetPasswordItem(clsOntologyItem passwordItem)
        {
            this.passwordItem = passwordItem;
        }

        public clsOntologyItem GetPasswordItem()
        {
            return this.passwordItem;
        }

    }
}
