﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Models
{
    public class SecuritySession
    {
        public clsOntologyItem UserItem { get; set; }
        public clsOntologyItem GroupItem { get; set; }
        public clsOntologyItem PassswortItem { get; set; }
        public clsOntologyItem EmailAddressItem { get; set; }
    }
}
