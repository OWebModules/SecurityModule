﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Models
{
    public class User
    {
        public string IdUser { get; set; }
        public string NameUser { get; set; }
    }
}
