﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using SecurityModule.Models;
using SecurityModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule
{
    public class SecurityController : AppController
    {
        private ElasticAgent elasticService;

        private CredentialItem credentialItem;
        public CredentialItem CredentialItem
        {
            get
            {
                return credentialItem;
            }
        }

        public bool IsAuthenticated
        {
            get { return credentialItem != null && credentialItem.Password != null; }
        }

        public async Task<ResultItem<string>> DecodePassword(string idPassword)
        {
            var result = new ResultItem<string>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = ""
            };

            var resultOItem = await elasticService.GetOItem(idPassword, Globals.Type_Object);

            result.ResultState = resultOItem.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var decodedPassword = EncryptDecryptController.DecodeString(resultOItem.Result.Name, credentialItem.Password.Name_Other);

            result.Result = decodedPassword;
            
            return result;
        }

        public async Task<ResultCredentials> GetPassword(clsOntologyItem userItem, string masterPassword, string idPasswordClass = "3e1587d83e3546dd983341903904254d")
        {
            var resultCredentials = new ResultCredentials
            {
                Result = Globals.LState_Success.Clone()
            };

            var resultTask = await elasticService.GetCredentials(userItem, false, idPasswordClass);

            if (resultTask.Result.GUID == Globals.LState_Error.GUID)
            {
                resultCredentials.Result = resultTask.Result;
                return resultCredentials;
            }

            credentialItem = resultTask.CredentialItems.FirstOrDefault();
            if (credentialItem == null)
            {
                resultCredentials.Result = Globals.LState_Error.Clone();
            }
            else
            {
                credentialItem.Password.Name_Other = EncryptDecryptController.DecodeString(credentialItem.Password.Name_Other, masterPassword);
                resultCredentials.CredentialItems = new List<CredentialItem> { credentialItem };
            }

            return resultCredentials;
        }

        public async Task<ResultCredentials> ValidatePassword(clsOntologyItem userItem, string password, string idPasswordClass = "3e1587d83e3546dd983341903904254d")
        {
            var resultCredentials = new ResultCredentials
            {
                Result = Globals.LState_Success.Clone()
            };

            var resultTask = await elasticService.GetCredentials(userItem, idPasswordClass: idPasswordClass);

            if (resultTask.Result.GUID == Globals.LState_Error.GUID)
            {
                resultCredentials.Result = resultTask.Result;
                return resultCredentials;
            }

            credentialItem = resultTask.CredentialItems.FirstOrDefault();
            if (credentialItem == null)
            {
                resultCredentials.Result = Globals.LState_Error.Clone();
            }
            else
            {
                try
                {
                    credentialItem.Password.Name_Other = EncryptDecryptController.DecodeString(credentialItem.Password.Name_Other, password);

                    if (credentialItem.Password.Name_Other != password)
                    {
                        credentialItem = null;
                        resultCredentials.Result = Globals.LState_Error.Clone();

                    }
                    else
                    {
                        resultCredentials.CredentialItems = new List<CredentialItem> { credentialItem };
                    }
                }
                catch (Exception ex)
                {
                    credentialItem = null;
                    resultCredentials.Result = Globals.LState_Error.Clone();

                }
                
            }

            return resultCredentials;
        }
        public async Task<ResultCredentials> Login(string emailAddress, string password)
        {
            var resultCredentials = new ResultCredentials
            {
                Result = Globals.LState_Success.Clone()
            };

            var resultTask = await elasticService.GetCredentials(emailAddress);


            if (resultTask.Result.GUID == Globals.LState_Error.GUID)
            {
                resultCredentials.Result = resultTask.Result;
                return resultCredentials;
            }

            credentialItem = resultTask.CredentialItems.FirstOrDefault();
            if (credentialItem == null)
            {
                resultCredentials.Result = Globals.LState_Error.Clone();
            }
            else
            {
                credentialItem.Password.Name_Other = EncryptDecryptController.DecodeString(credentialItem.Password.Name_Other, password);
                
                if (credentialItem.Password.Name_Other != password)
                {
                    resultCredentials.Result = Globals.LState_Error.Clone();
                    

                }
                else
                {
                    resultCredentials.CredentialItems = new List<CredentialItem> { credentialItem };
                }
            }

            

            return resultCredentials;
        }

        public async Task<ResultCredentials> LoginWithUser(string idUser, string password)
        {
            var resultCredentials = new ResultCredentials
            {
                Result = Globals.LState_Success.Clone()
            };

            var resultUser = await elasticService.GetOItem(idUser, Globals.Type_Object);

            if (resultUser.ResultState.GUID == Globals.LState_Error.GUID)
            {
                resultCredentials.Result = resultUser.ResultState;
                return resultCredentials;
            }

            var resultTask = await elasticService.GetCredentials(resultUser.Result);


            if (resultTask.Result.GUID == Globals.LState_Error.GUID)
            {
                resultCredentials.Result = resultTask.Result;
                return resultCredentials;
            }

            credentialItem = resultTask.CredentialItems.FirstOrDefault();
            if (credentialItem == null)
            {
                resultCredentials.Result = Globals.LState_Error.Clone();
            }
            else
            {
                credentialItem.Password.Name_Other = EncryptDecryptController.DecodeString(credentialItem.Password.Name_Other, password);

                if (credentialItem.Password.Name_Other != password)
                {
                    resultCredentials.Result = Globals.LState_Error.Clone();


                }
                else
                {
                    resultCredentials.CredentialItems = new List<CredentialItem> { credentialItem };
                }
            }



            return resultCredentials;
        }

        public async Task<ResultGetUser> GetUser(string userId)
        {
            var resultTask = await elasticService.GetUser(userId);

            var result = new ResultGetUser
            {
                Result = resultTask.Result,
                UserItem = resultTask.UserItem
            };

            return result;
        }

        public async Task<ResultGetGroup> GetGroup(string userId)
        {
            var resultTask = await elasticService.GetGroup(userId);

            var result = new ResultGetGroup
            {
                Result = resultTask.Result,
                GroupItem = resultTask.GroupItem
            };

            return result;
        }

        public async Task<ResultItem<List<PasswordSafeItem>>> GetEmptyPasswordSafeItemList()
        {
            var taskResult = await Task.Run<ResultItem<List<PasswordSafeItem>>>(() =>
            {
                var result = new ResultItem<List<PasswordSafeItem>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<PasswordSafeItem>()
                };

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<PasswordSafeItem>> SavePassword(clsOntologyItem refItem, string password, string idPassword = null)
        {
            var taskResult = await Task.Run<ResultItem<PasswordSafeItem>>(async() =>
            {
                var transaction = new clsTransaction(Globals);
                var relationConfig = new clsRelationConfig(Globals);

                var result = new ResultItem<PasswordSafeItem>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var encodingTypeResult = await elasticService.GetEncodingType();

                result.ResultState = encodingTypeResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                var encodedPassword = EncryptDecryptController.EncodeString(password, credentialItem.Password.Name_Other);

                var passwordItem = new clsOntologyItem
                {
                    GUID = !string.IsNullOrEmpty(idPassword) ? idPassword : Globals.NewGUID,
                    Name = encodedPassword,
                    GUID_Parent = encodingTypeResult.Result.GUID,
                    Type = Globals.Type_Object
                };

                result.ResultState = transaction.do_Transaction(passwordItem);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
                if (string.IsNullOrEmpty(idPassword))
                {
                    var passwordRel = relationConfig.Rel_ObjectRelation(refItem, passwordItem, Config.LocalData.RelationType_secured_by);

                    result.ResultState = transaction.do_Transaction(passwordRel);

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        transaction.rollback();

                        return result;
                    }
                }
                
                result.Result = new PasswordSafeItem
                {
                    IdReference = refItem.GUID,
                    IdPassword = passwordItem.GUID,
                    IdParentReference = refItem.GUID,
                    NameReference = refItem.Name,
                    Password = passwordItem.Name
                };
                result.Result.SetPasswordItem(passwordItem);

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<PasswordSafeItem>>> GetPasswordSafeItems(clsOntologyItem refItem, clsOntologyItem refParentItem)
        {
            var resultGetPasswordSafeItems = await elasticService.GetPasswordSafeItems(refItem);

            var result = new ResultItem<List<PasswordSafeItem>>
            {
                ResultState = resultGetPasswordSafeItems.ResultState
            };

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            var factory = new Factories.PasswordSafeItemsFactory(Globals);

            var resultFactory = await factory.CreatePasswordSafeItemsList(resultGetPasswordSafeItems.Result, refParentItem);

            result.ResultState = resultFactory.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            result.Result = resultFactory.Result;

            return result;
        }

        public async Task<ResultItem<SecuritySession>> GetSecuritySession()
        {
            var taskResult = await Task.Run<ResultItem<SecuritySession>>(() =>
            {
                var result = new ResultItem<SecuritySession>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new SecuritySession()
                };

                var elasticAgent = new ElasticAgent(Globals);



                return result;
            });

            return taskResult;
        }

        
        public SecurityController(Globals globals) : base(globals)
        {
            Initialize();
        }

        private void Initialize()
        {
            elasticService = new ElasticAgent(Globals);
        }
    }

    
}
