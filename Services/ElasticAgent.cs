﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecurityModule.Services
{
    public class ElasticAgent : NotifyPropertyChange
    {
        private object serviceLocker = new object();

        private Globals globals;

        private ResultCredentials resultCredentials;
        public ResultCredentials ResultCredentials
        {
            get
            {
                lock (serviceLocker)
                {
                    return resultCredentials;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    resultCredentials = value;
                }

                RaisePropertyChanged(nameof(ResultCredentials));
            }
        }

        private ResultGetUser resultGetUser;
        public ResultGetUser ResultGetUser
        {
            get
            {
                lock (serviceLocker)
                {
                    return resultGetUser;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    resultGetUser = value;
                }

                RaisePropertyChanged(nameof(ResultGetUser));
            }
        }

        private ResultGetGroup resultGetGroup;
        public ResultGetGroup ResultGetGroup
        {
            get
            {
                lock (serviceLocker)
                {
                    return resultGetGroup;
                }
            }
            set
            {
                lock (serviceLocker)
                {
                    resultGetGroup = value;
                }

                RaisePropertyChanged(nameof(ResultGetGroup));
            }
        }

        public async Task<ResultGetUser> GetUser(string userId)
        {
            var taskResult = await Task.Run<ResultGetUser>(() =>
           {
               var result = new ResultGetUser
               {
                   Result = globals.LState_Success.Clone()
               };

               var searchUser = new List<clsOntologyItem> { new clsOntologyItem
                {
                    GUID = userId,
                    GUID_Parent = Config.LocalData.Class_user.GUID
                }
           };

               var dbReaderUser = new OntologyModDBConnector(globals);
               result.Result = dbReaderUser.GetDataObjects(searchUser);
               if (result.Result.GUID == globals.LState_Error.GUID)
               {
                   ResultGetUser = result;
                   return result;
               }

               result.UserItem = dbReaderUser.Objects1.FirstOrDefault();

               ResultGetUser = result;
               return result;
           });

            return taskResult;
            
        }

        public async Task<ResultGetGroup> GetGroup(string userId)
        {
            var taskResult = await Task.Run<ResultGetGroup>(() =>
            {
                var result = new ResultGetGroup
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchGroup = new List<clsObjectRel> { new clsObjectRel
                {
                    ID_Other = userId,
                    ID_RelationType = Config.LocalData.ClassRel_Group_contains_user.ID_RelationType,
                    ID_Parent_Object = Config.LocalData.ClassRel_Group_contains_user.ID_Class_Left
                }
            };

                var dbReaderUser = new OntologyModDBConnector(globals);
                result.Result = dbReaderUser.GetDataObjectRel(searchGroup);
                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultGetGroup = result;
                    return result;
                }

                result.GroupItem = dbReaderUser.ObjectRels.Select(usrGrpRel => new clsOntologyItem
                {
                    GUID = usrGrpRel.ID_Object,
                    Name = usrGrpRel.Name_Object,
                    GUID_Parent = usrGrpRel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                ResultGetGroup = result;
                return result;
            });

            return taskResult;
            
        }

        public async Task<ResultItem<clsOntologyItem>> GetOItem(string idOItem, string type)
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var dbReader = new OntologyModDBConnector(globals);
                var resultGetOItem = dbReader.GetOItem(idOItem, type);

                result.ResultState = globals.LogStates.LogStates.First(logState => logState.GUID == resultGetOItem.GUID_Related);

                result.Result = resultGetOItem;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<PasswordSafeRelItems>> GetPasswordSafeItems(clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<ResultItem<PasswordSafeRelItems>>(() =>
            {
                var result = new ResultItem<PasswordSafeRelItems>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                result.Result = new PasswordSafeRelItems();

                if (refItem.GUID_Parent != Config.LocalData.Class_user.GUID)
                {
                    var searchUsersRightLeft = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Other = refItem.GUID,
                            ID_Parent_Object = Config.LocalData.Class_user.GUID
                        }
                    };

                    var dbReaderUsersRightLeft = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderUsersRightLeft.GetDataObjectRel(searchUsersRightLeft);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchUsersLeftRight = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = refItem.GUID,
                            ID_Parent_Other = Config.LocalData.Class_user.GUID
                        }
                    };

                    var dbReaderUsersLeftRight = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderUsersLeftRight.GetDataObjectRel(searchUsersLeftRight);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchPasswordsOfUsersLeftRight = dbReaderUsersLeftRight.ObjectRels.Select(objRel => new clsObjectRel
                    {
                        ID_Object = objRel.ID_Other,
                        ID_RelationType = Config.LocalData.RelationType_secured_by.GUID
                    }).ToList();


                    var dbReaderPassworsOfUsersLeftRight = new OntologyModDBConnector(globals);

                    if (searchPasswordsOfUsersLeftRight.Any())
                    {
                        result.ResultState = dbReaderPassworsOfUsersLeftRight.GetDataObjectRel(searchPasswordsOfUsersLeftRight);
                    }

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    var searchPasswordsOfUsersRightLeft = dbReaderUsersRightLeft.ObjectRels.Select(objRel => new clsObjectRel
                    {
                        ID_Object = objRel.ID_Object,
                        ID_RelationType = Config.LocalData.RelationType_secured_by.GUID
                    }).ToList();


                    var dbReaderPassworsOfUsersRightLeft = new OntologyModDBConnector(globals);

                    if (searchPasswordsOfUsersRightLeft.Any())
                    {
                        result.ResultState = dbReaderPassworsOfUsersRightLeft.GetDataObjectRel(searchPasswordsOfUsersRightLeft);
                    }

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    result.Result.UsersRightLeft = dbReaderUsersRightLeft.ObjectRels;
                    result.Result.UsersLeftRight = dbReaderUsersLeftRight.ObjectRels;
                    result.Result.PasswordsOfUsersLeftRight = dbReaderPassworsOfUsersLeftRight.ObjectRels.Where(passwd => passwd.ID_Object != refItem.GUID).ToList();
                    result.Result.PasswordsOfUsersRightLeft = dbReaderPassworsOfUsersRightLeft.ObjectRels.Where(passwd => passwd.ID_Other != refItem.GUID).ToList();
                }



                var searchPasswordsOfRef = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = refItem.GUID,
                        ID_RelationType = Config.LocalData.RelationType_secured_by.GUID                    }
                };

                var dbReaderPasswords = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderPasswords.GetDataObjectRel(searchPasswordsOfRef);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.Result.RefItem = refItem;
                result.Result.PasswordsOfRefItem = dbReaderPasswords.ObjectRels.Where(usr => usr.ID_Parent_Other !=  Config.LocalData.Class_user.GUID).ToList();

                return result;
            });

            return taskResult;
        }

        public async Task<ResultCredentials> GetCredentials(clsOntologyItem userItem, bool withEmail = true, string idPasswordClass = "3e1587d83e3546dd983341903904254d")
        {
            var taskResult = await Task.Run<ResultCredentials>(() =>
            {
                var result = new ResultCredentials
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchGroup = new List<clsObjectRel>{new clsObjectRel
                {
                    ID_Other = userItem.GUID,
                    ID_Parent_Object = Config.LocalData.ClassRel_Group_contains_user.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Group_contains_user.ID_RelationType
                } };


                var dbReaderGroup = new OntologyModDBConnector(globals);

                if (searchGroup.Any())
                {


                    result.Result = dbReaderGroup.GetDataObjectRel(searchGroup);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultCredentials = result;
                        return result;
                    }


                }

                var searchPassword = new List<clsObjectRel>{ new clsObjectRel
                {
                    ID_Object = userItem.GUID,
                    ID_RelationType = Config.LocalData.RelationType_secured_by.GUID,
                    ID_Parent_Other = idPasswordClass
                } };

                var dbReaderPassword = new OntologyModDBConnector(globals);

                if (searchPassword.Any())
                {


                    result.Result = dbReaderPassword.GetDataObjectRel(searchPassword);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultCredentials = result;
                        return result;
                    }


                }

                var searchEmailAddress = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = userItem.GUID,
                        ID_RelationType = globals.RelationType_belongsTo.GUID,
                        ID_Parent_Object = Config.LocalData.Class_eMail_Address.GUID
                    }
                };

                var dbReaderEmailAddress = new OntologyModDBConnector(globals);

                result.Result = dbReaderEmailAddress.GetDataObjectRel(searchEmailAddress);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultCredentials = result;
                    return result;
                }


                if (withEmail)
                {
                    result.CredentialItems = (from email in dbReaderEmailAddress.ObjectRels
                                              join user in new List<clsOntologyItem> { userItem } on email.ID_Other equals user.GUID
                                              join grp in dbReaderGroup.ObjectRels on user.GUID equals grp.ID_Other into grps
                                              from grp in grps.DefaultIfEmpty()
                                              join pwd in dbReaderPassword.ObjectRels on user.GUID equals pwd.ID_Object
                                              select new CredentialItem
                                              {
                                                  EmailAddress = new clsOntologyItem
                                                  {
                                                      GUID = email.ID_Object,
                                                      Name = email.Name_Object,
                                                      GUID_Parent = email.ID_Parent_Object,
                                                      Type = globals.Type_Object
                                                  },
                                                  User = userItem,
                                                  Group = grp != null ? new clsOntologyItem
                                                  {
                                                      GUID = grp.ID_Object,
                                                      Name = grp.Name_Object,
                                                      GUID_Parent = grp.ID_Parent_Object,
                                                      Type = globals.Type_Object
                                                  } : null,
                                                  Password = pwd
                                              }).ToList();
                }
                else
                {
                    result.CredentialItems = (from user in new List<clsOntologyItem> { userItem } 
                                              join grp in dbReaderGroup.ObjectRels on user.GUID equals grp.ID_Other into grps
                                              from grp in grps.DefaultIfEmpty()
                                              join pwd in dbReaderPassword.ObjectRels on user.GUID equals pwd.ID_Object
                                              select new CredentialItem
                                              {
                                                  EmailAddress = null,
                                                  User = userItem,
                                                  Group = grp != null ? new clsOntologyItem
                                                  {
                                                      GUID = grp.ID_Object,
                                                      Name = grp.Name_Object,
                                                      GUID_Parent = grp.ID_Parent_Object,
                                                      Type = globals.Type_Object
                                                  } : null,
                                                  Password = pwd
                                              }).ToList();
                }
                

                ResultCredentials = result;
                return result;
            });
            return taskResult;
        }

        

        public async Task<ResultItem<clsOntologyItem>> GetEncodingType()
        {
            var taskResult = await Task.Run<ResultItem<clsOntologyItem>>(() =>
            {
                var result = new ResultItem<clsOntologyItem>
                {
                    ResultState = globals.LState_Success.Clone()
                };

                var searchBaseConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = Config.LocalData.Object_Security_Module.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Security_Module_belongs_to_Module.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Security_Module_belongs_to_Module.ID_Class_Left
                    }
                };

                var dbReaderBaseConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderBaseConfig.GetDataObjectRel(searchBaseConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the base-config!";
                    return result;
                }

                var baseConfigToModule = dbReaderBaseConfig.ObjectRels.OrderBy(rel => rel.OrderID).FirstOrDefault();

                if (baseConfigToModule == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Baseconfig found!";
                    return result;
                }

                var searchEncodingType = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = baseConfigToModule.ID_Object,
                        ID_RelationType = Config.LocalData.RelationType_belonging_Endoding_Types.GUID
                    }
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjectRel(searchEncodingType);

                if (result.ResultState.GUID == globals.LState_Error.GUID || !dbReader.ObjectRels.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Encoding-Class found!";
                    return result;
                }

                result.Result = dbReader.ObjectRels.OrderBy(encType => encType.OrderID).Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = rel.Ontology
                }).FirstOrDefault();

                if (result.Result == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Encoding-Class found!";
                    return result;
                }

                return result;
            });
            return taskResult;
        }


        public async Task<ResultCredentials> GetCredentials(string emailAddress)
        {
            var taskResult = await Task.Run<ResultCredentials>(async() =>
            {
                var result = new ResultCredentials
                {
                    Result = globals.LState_Success.Clone()
                };

                var searchEmailAddress = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        Name = emailAddress,
                        GUID_Parent = Config.LocalData.Class_eMail_Address.GUID
                    }
                };

                var dbReaderEmailAddress = new OntologyModDBConnector(globals);

                result.Result = dbReaderEmailAddress.GetDataObjects(searchEmailAddress);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    ResultCredentials = result;
                    return result;
                }

                var searchUser = dbReaderEmailAddress.Objects1.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_eMail_Address_belongs_to_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_eMail_Address_belongs_to_user.ID_Class_Right
                }).ToList();

                var dbReaderUser = new OntologyModDBConnector(globals);

                if (searchUser.Any())
                {
                    result.Result = dbReaderUser.GetDataObjectRel(searchUser);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultCredentials = result;
                        return result;
                    }
                }


                var searchGroup = dbReaderUser.ObjectRels.Select(obj => new clsObjectRel
                {
                    ID_Other = obj.ID_Other,
                    ID_Parent_Object = Config.LocalData.ClassRel_Group_contains_user.ID_Class_Left,
                    ID_RelationType = Config.LocalData.ClassRel_Group_contains_user.ID_RelationType
                }).ToList();


                var dbReaderGroup = new OntologyModDBConnector(globals);

                if (searchGroup.Any())
                {


                    result.Result = dbReaderGroup.GetDataObjectRel(searchGroup);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultCredentials = result;
                        return result;
                    }


                }

                var encodingTypeResult = await GetEncodingType();
                result.Result = encodingTypeResult.ResultState;
                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchPassword = dbReaderUser.ObjectRels.Select(obj => new clsObjectRel
                {
                    ID_Object = obj.ID_Other,
                    ID_RelationType = Config.LocalData.RelationType_secured_by.GUID,
                    ID_Parent_Other = encodingTypeResult.Result.GUID
                }).ToList();

                var dbReaderPassword = new OntologyModDBConnector(globals);

                if (searchPassword.Any())
                {


                    result.Result = dbReaderPassword.GetDataObjectRel(searchPassword);

                    if (result.Result.GUID == globals.LState_Error.GUID)
                    {
                        ResultCredentials = result;
                        return result;
                    }


                }

                result.CredentialItems = (from email in dbReaderEmailAddress.Objects1
                                          join user in dbReaderUser.ObjectRels on email.GUID equals user.ID_Object
                                          join grp in dbReaderGroup.ObjectRels on user.ID_Other equals grp.ID_Other into grps
                                          from grp in grps.DefaultIfEmpty()
                                          join pwd in dbReaderPassword.ObjectRels on user.ID_Other equals pwd.ID_Object
                                          select new CredentialItem
                                          {
                                              EmailAddress = email,
                                              User = user != null ? new clsOntologyItem
                                              {
                                                  GUID = user.ID_Other,
                                                  Name = user.Name_Other,
                                                  GUID_Parent = user.ID_Parent_Other,
                                                  Type = user.Ontology
                                              } : null,
                                              Group = grp != null ? new clsOntologyItem
                                              {
                                                  GUID = grp.ID_Object,
                                                  Name = grp.Name_Object,
                                                  GUID_Parent = grp.ID_Parent_Object,
                                                  Type = globals.Type_Object
                                              } : null,
                                              Password = pwd
                                          }).ToList();

                ResultCredentials = result;
                return result;
            });
            return taskResult;
        }

        public async Task<ResultItem<GetSecuritySessionsResult>> GetSecuritySessions()
        {
            var taskResult = await Task.Run<ResultItem<GetSecuritySessionsResult>>(() =>
            {
                var result = new ResultItem<GetSecuritySessionsResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetSecuritySessionsResult()
                };

                var computer = globals.OItem_Server;

                var dbReaderGetSecuritySession = new OntologyModDBConnector(globals);

                var searchSecuritySessions = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = computer.GUID,
                        ID_RelationType = Config.LocalData.ClassRel_Security_Session_belongs_to_Server.ID_RelationType,
                        ID_Parent_Object = Config.LocalData.ClassRel_Security_Session_belongs_to_Server.ID_Class_Left
                    }
                };

                result.ResultState = dbReaderGetSecuritySession.GetDataObjectRel(searchSecuritySessions);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Security-Sessions!";
                    return result;
                }

                result.Result.SessionsToServers = dbReaderGetSecuritySession.ObjectRels;

                result.Result.SecuritySessions = result.Result.SessionsToServers.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).ToList();

                var searchUser = result.Result.SecuritySessions.Select(sec => new clsObjectRel
                {
                    ID_Object = sec.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Security_Session_belongs_to_user.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Security_Session_belongs_to_user.ID_Class_Right
                }).ToList();

                var dbReaderUser = new OntologyModDBConnector(globals);

                if (searchUser.Any())
                {
                    result.ResultState = dbReaderUser.GetDataObjectRel(searchUser);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the User!";
                        return result;
                    }
                }
                
                result.Result.SessionsToUsers = dbReaderUser.ObjectRels;

                var searchGroup = result.Result.SecuritySessions.Select(sec => new clsObjectRel
                {
                    ID_Object = sec.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Security_Session_belongs_to_Group.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Security_Session_belongs_to_Group.ID_Class_Right
                }).ToList();


                var dbReaderGroup = new OntologyModDBConnector(globals);

                if (searchGroup.Any())
                {
                    result.ResultState = dbReaderGroup.GetDataObjectRel(searchGroup);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the group!";
                        return result;
                    }
                }

                result.Result.SessionsToGroups = dbReaderGroup.ObjectRels;

                var searchGroupsToUsers = (from secSessToUser in result.Result.SessionsToUsers
                                           from secSessToGroup in result.Result.SessionsToUsers
                                           select new clsObjectRel
                                           {
                                               ID_Object = secSessToGroup.ID_Other,
                                               ID_Other = secSessToUser.ID_Other,
                                               ID_RelationType = Config.LocalData.ClassRel_Group_contains_user.ID_RelationType
                                           }).ToList();

                return result;
           });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }
    }

    

    public class ResultGetUser
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem UserItem { get; set; }
    }

    public class ResultGetGroup
    {
        public clsOntologyItem Result { get; set; }
        public clsOntologyItem GroupItem { get; set; }
    }

    public class ResultCredentials
    {
        public clsOntologyItem Result { get; set; }
        public List<CredentialItem> CredentialItems { get; set; }
    }
    public class CredentialItem
    {
        public clsOntologyItem User { get; set; }
        public clsOntologyItem EmailAddress { get; set; }
        public clsOntologyItem Group { get; set; }
        public clsObjectRel Password { get; set; }
    }

    public class PasswordSafeRelItems
    {
        public clsOntologyItem RefItem { get; set; } = new clsOntologyItem();
        public List<clsObjectRel> UsersLeftRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> UsersRightLeft { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PasswordsOfRefItem { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PasswordsOfUsersLeftRight { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> PasswordsOfUsersRightLeft { get; set; } = new List<clsObjectRel>();
    }
}
